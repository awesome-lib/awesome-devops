Devops 资源大全
==============

有关 DevOps 的精选资源大全

什么是 DevOps?
---------------

DevOps（“development”和“operations”的复合词）是一种文化、运动或实践，它强调软件开发人员和其他 IT 人员之间的协作和沟通，同时使软件交付和基础架构更改的过程自动化 。

[DevOps 的百度百科](https://baike.baidu.com/item/DevOps/2613029) 


目录
-----

-	[DevOps 文化](#DevOps文化)
-	[DevOps 流程](#DevOps流程)
-	[DevOps 相关技术](#DevOps相关技术)
-	[DevOps 的安全问题](#DevOps的安全问题)
-	[DevOps 工具](#DevOps工具)
-	[其他](#其他)

DevOps文化
-------

-	[什么是 DevOps 文化？](https://gitee.com/awesome-lib/awesome-devops/blob/master/DevOps%E6%96%87%E5%8C%96/%E4%BB%80%E4%B9%88%E6%98%AF%20DevOps%20%E7%9A%84%E6%96%87%E5%8C%96%EF%BC%9F.md)
-	[康威定律](https://gitee.com/awesome-lib/awesome-devops/blob/master/DevOps%E6%96%87%E5%8C%96/%E5%BA%B7%E5%A8%81%E5%AE%9A%E5%BE%8B.md) - "设计系统的架构受制于产生这些设计的组织的沟通结构。"
-	[How to Hire](https://medium.com/swlh/how-to-hire-34f4ded5f176#.ilxplhbdh) - 一些有用的招聘建议
-	[What security experts need to know about DevOps and continuous delivery](https://labs.signalsciences.com/what-security-experts-need-to-know-about-devops-and-continuous-delivery-f9e0d53dd09f#.7y0lxtsr9) - 安全团队和 DevOps 团队一起工作优势
-	DevOps and the Myth of Efficiency [Part 1](http://blog.christianposta.com/devops/devops-and-the-myth-of-efficiency-part-i/) & [Part 2](http://blog.christianposta.com/devops/devops-and-the-myth-of-efficiency-part-ii/) - 企业的 DevOps - 更复杂还是复杂且高效
-   [Who drives culture in DevOps?](https://opensource.com/article/17/12/who-drives-culture-devops)

DevOps流程
-------

-	[The War of Independence for Enterprise Architecture](https://medium.com/compliance-at-velocity/the-war-of-independence-for-enterprise-architecture-1ed8eb34af3f#.kts5s5a12) - 架构师在 DevOps 中的角色。
-	[Choosing Design over Architecture](https://18f.gsa.gov/2015/11/17/choose-design-over-architecture/) - 从用户故事和用户体验出发。
-	[How to write a Postmortem](https://blog.serverdensity.com/how-to-write-a-postmortem/)

### 项目管理

-	[Gitee 企业版](https://gitee.com/enterprises) - Gitee 企业版提供非常简单易用、具备实用定制能力的项目协作管理工具。
-	[Organizing GitHub issues](https://robinpowered.com/blog/best-practice-system-for-organizing-and-tagging-github-issues/) - 如何对 GitHub Issue 进行有效管理。
-	[Release Ready Teams](https://www.atlassian.com/agile/release-ready-agile-teams) - 一张信息图，展示了使用 Atlassian 的敏捷团队如何工作。

-	[Using Kanban over Scrum](https://medium.com/cto-school/ditching-scrum-for-kanban-the-best-decision-we-ve-made-as-a-team-cd1167014a6f#.p8a1zicwm) - Kanban 方法在敏捷中的妙用。

### DevOps 图谱

-	[Wardley Mapping](http://blog.gardeviance.org/2015/02/an-introduction-to-wardley-value-chain.html) - 通过价值链的方式帮助你理解 DevOps 团队中的那些「为什么」。

### 自动化

### 质量管理

### 开源

-	[Making Your Open Source Project Newcomer-friendly](http://manishearth.github.io/blog/2016/01/03/making-your-open-source-project-newcomer-friendly/)

DevOps相关技术
----------

## 云计算

#### IaaS（基础架构即服务）


-	[CloudStack](https://cloudstack.apache.org/) ([`开源仓库`](https://gitee.com/apache/cloudstack)) Apache CloudStack 是一个开源的具有高可用性及扩展性的云计算平台，同时是一个开源云计算解决方案。可以加速高伸缩性的公共和私有云（IaaS）的部署、管理、配置。

-	[OpenNebula](http://opennebula.org/) ([`开源仓库`](https://github.com/OpenNebula)) 构建云和管理数据中心虚拟化的包解决方案。 


-	[Openstack](https://www.openstack.org/) ([`开源仓库`](https://github.com/openstack)) OpenStack 为私有云和公有云提供可扩展的弹性的云计算服务。项目目标是提供实施简单、可大规模扩展、丰富、标准统一的云计算管理平台。

-	[Proxmox VE](https://www.proxmox.com/en/proxmox-ve) ([`开源仓库`](https://git.proxmox.com))一个完整的开源服务器虚拟化管理软件，基于 KVM 和容器的虚拟化，管理虚拟机、Linux 容器、存储、虚拟化网络和 HA 集群。 

-	[ZStack](http://www.zstack.io/) ([`开源仓库`](https://gitee.com/zstackio/zstack)) ZStack 是一款产品化的开源 IaaS（基础架构即服务）软件。它面向智能数据中心，通过完善的 API 统一管理包括计算、存储和网络在内的数据中心资源，提供简单快捷的环境搭建。

#### PaaS（平台即服务）

-	[Cloud Foundry](https://www.cloudfoundry.org/) ([`开源仓库`](https://github.com/cloudfoundry)) Cloud Foundry 是业界第一个开源 PaaS 云平台，它支持多种框架、语言、运行时环境、云平台及应用服务，使开发人员能够在几秒钟内进行应用程序的部署和扩展，无需担心任何基础架构的问题。

-	[Cloudify](http://getcloudify.org/) ([`开源仓库`](https://github.com/cloudify-cosmo)) Cloudify 是一个云应用的编排系统，可让你的应用自动化的在各种不同的云上方便的部署。

-	[Cloudron](https://cloudron.io/) ([`开源仓库`](https://git.cloudron.io/cloudron/box)) Cloudron 是一个简化应用程序到您服务器的安装和管理的平台。

-	[Convox](https://convox.com/) ([`开源仓库`](https://github.com/convox/rack)) 轻松构建、部署和管理应用程序。 


-	[Dokku](http://dokku.viewdocs.io/dokku/) ([`开源仓库`](https://github.com/dokku/dokku))Dokku 是一个微型的 Heroku，由 Docker 使用不多于 100 行的 Bash 编写。一旦安装完成，你就可以通过 Git 推送兼容 Heroku 的应用到平台上运行。

-	[Openshift Origin](https://www.openshift.org/) ([`开源仓库`](https://github.com/openshift/origin)) OpenShift 是红帽的云开发平台即服务（PaaS）。自由和开放源码的云计算平台使开发人员能够创建、测试和运行他们的应用程序，并且可以把它们部署到云中。

-	[Tsuru](https://tsuru.io/) ([`开源仓库`](https://github.com/tsuru)) Tsuru 是一个开源的 PaaS 平台。Tsuru 可以让你构建自己的 PaaS 服务。Tsuru 采用 go 语言写成，依赖 go 环境和 libxml。

#### CaaS/FaaS（容器即服务/功能即服务）

-	[Apache OpenWhisk](http://openwhisk.incubator.apache.org) ([`开源仓库`](https://github.com/apache/openwhisk)) Apache OpenWhisk 是一个开放源代码的分布式无服务器平台，该平台可以执行功能（fx）以响应各种规模的事件。

-	[Fission](http://fission.io/) ([`开源仓库`](https://github.com/fission/fission)) Fission 是 Kubernetes 上的 Serverless 框架。 


-	[OpenFaaS](https://www.openfaas.com/) ([`开源仓库`](https://github.com/openfaas/faas)) OpenFaaS 是一个使用 Docker 构建无服务器(Serverless)功能的框架，它拥有对指标的一级支持。


-	[OpenLambda](https://github.com/open-lambda/open-lambda) ([`开源仓库`](https://github.com/open-lambda/open-lambda)) 一个 Serverless 计算项目，用 Go 编写并基于 Linux 容器。 



## 持续集成和持续部署（CI/CD）

-	[Abstruse](https://abstruse.bleenco.io/) ([`开源仓库`](https://github.com/bleenco/abstruse)) 一个需要零配置或最少配置即可启动的持续集成平台，使用 Docker 容器提供安全的测试和部署环境。 

-	[Buildbot](https://buildbot.net/) ([`开源仓库`](https://github.com/buildbot/buildbot)) Buildbot 是一个用于自动化软件构建、测试和发布过程的开源框架。

-	[Concourse](http://concourse.ci/) ([`开源仓库`](https://github.com/concourse/concourse)) Concourse 是一个用 Go 编写的基于流水线的 CI 系统。 

-	[Deployer](https://deployer.org/) ([`开源仓库`](https://github.com/deployphp/deployer)) Deployer 是一个 PHP 写的部署工具。


-	[Drone](https://drone.io/) ([`开源仓库`](https://github.com/drone/drone)) Drone 是一个基于 Docker 的持续交付平台，用 Go 语言编写。 

-	[Gitlab CI](https://about.gitlab.com/gitlab-ci/) ([`开源仓库`](https://gitlab.com/gitlab-org/gitlab-ci)) GitLab 的 CI/CD 服务。

-	[GO CD](https://www.go.cd/) ([`开源仓库`](https://github.com/gocd/gocd)) 一款持续集成和发布管理系统，由 ThoughtWorks 开发。

-	[Jenkins](https://jenkins.io/) ([`开源仓库`](https://github.com/jenkinsci/jenkins)) Jenkins 是一款开源 CI/CD 软件，用于自动化各种任务，包括构建、测试和部署软件。

-	[Strider](https://strider-cd.github.io/) ([`开源仓库`](https://github.com/Strider-CD/strider)) Strider 是一个开源的持续集成和发布服务器。使用 Node.js 开发。

-	[TeamCity](https://www.jetbrains.com/teamcity/) (`免费`) JetBrains 出品的持续集成服务。


### 容器

-	[The Curious Case of Linux Containers](https://medium.com/@sumbry/the-curious-case-of-linux-containers-328e2adc12a2#.j1hbq72im) - 一篇讨论跨分布式系统部署容器的实际问题的博客文章。
-	[The Oncoming Train of Enterprise Container Deployments](http://www.juliandunn.net/2015/12/04/the-oncoming-train-of-enterprise-container-deployments/) - 容器和反面模式的解读。 
-	[DevOps, Containers & Microservices: Separating the hype from the reality](http://www.slideshare.net/dberkholz/devops-containers-microservices-separating-the-hype-from-the-reality) - 如何构建和部署应用程序以产生业务价值。 
- [A Practical Introduction to Docker Container Terminology](http://developerblog.redhat.com/2016/01/13/a-practical-introduction-to-docker-container-terminology/) - 在讨论容器化架构时，扎实掌握相关词汇非常重要。 


### 操作系统

-	[The Art of Command Line](https://github.com/jlevy/the-art-of-command-line) - 一份针对新手的命令行操作指南。

### 云

-	[Infrastructure as Database](http://www.scriptcrafty.com/infrastructure-as-a-database/) - 比起代码，数据库更有基础设施的「样子」吗？

### 微服务

### DevOps 的安全问题

-	[You Wouldn't Base64 a Password - Cryptography Decoded](https://paragonie.com/blog/2015/08/you-wouldnt-base64-a-password-cryptography-decoded) - 开发人员的密码学入门。

-	[How to Protect Your Infrastructure Against the Basic Attacker](http://blog.mailgun.com/security-guide-basic-infrastructure-security/) - 基于 Linux 的系统中重要安全配置的概述。


DevOps工具
-----

### 容器工具

-	[Docker](https://www.docker.com/) - 现代容器工具的开创者。

### 操作系统

-	[CoreOS](https://github.com/coreos) - Fedora CoreOS 是一个专门为安全和大规模运行容器化工作负载而构建的新 Fedora 版本，它是  Fedora Atomic Host 和 CoreOS Container Linux 的后续项目。
-	[k3OS](https://gitee.com/rancher/k3os) - k3OS 是 Linux 发行版，旨在消除 Kubernetes 集群中尽可能多的 OS 维护。

### 集群管理

-	[Kubernetes](https://kubernetes.io)
-	[Nomad](https://www.nomadproject.io/)
-	[Mesos](https://mesos.apache.org/)
-	[Mesosphere](https://mesosphere.com/)
-	[Swarm](https://docs.docker.com/swarm/)

### 代码管理

-	[Git](https://git-scm.com/) - 最流行的版本管理工具。
-	[Gitee](https://gitee.com/) - 中国最大的代码托管平台和研发效能平台。
-	[GitHub](https://github.com/) - 世界最大的代码托管服务平台。
-	[GitLab](https://about.gitlab.com/) - 可进行私有部署的 Git 管理平台。
-	[Gitea](https://gitea.com/) - 可进行私有部署的 Git 管理平台，使用 Go 语言编写。
-	[Gogs](https://gogs.io/) - 一款极易搭建的自助 Git 服务。
-	[Mercurial](https://www.mercurial-scm.org/) 分布式版本控制系统。

### 运维工具

-	[Ansible](http://www.ansible.com/)
-	[Chef](https://www.chef.io/)
-	[Puppet](https://puppetlabs.com/)
-	[SaltStack](https://saltstack.com/)

### 持续集成和开发

-	[Jenkins](https://jenkins-ci.org/)
-	[Buildkite](https://buildkite.com/)
-	[Drone](https://github.com/drone)
-	[Shippable](https://app.shippable.com/)
-	[Travis](https://travis-ci.org/)
-	[Gitlab CI](https://about.gitlab.com/)

### 事件管理

- [PagerTree](https://pagertree.com/)
- [OpsGenie](https://www.opsgenie.com/)
- [VictorOps](https://victorops.com/)
- [PagerDuty](https://www.pagerduty.com/)

其他
----

-	[Awesome Lists](https://github.com/sindresorhus/awesome) - A list of Awesome lists (very meta!)
-	[DevOps Weekly](http://www.devopsweekly.com/) - A weekly mailing list with interesting DevOps related News and Tools
-	[DevOpsLinks](http://devopslinks.com/) - A newsletter & team chat with interesting DevOps related News and Tools
-	[Sysadvent](http://sysadvent.blogspot.co.uk) - One DevOps/Sysadmin related article for each day of December, ending on the 25th article.
-	[The Phoenix Project](http://www.amazon.com/Phoenix-Project-DevOps-Helping-Business/dp/0988262509/ref=sr_1_1?ie=UTF8&qid=1451900824&sr=8-1&keywords=project+phoenix) - A Novel about IT, DevOps, and Helping Your Business Win
-   [DevOps'ish](https://devopsish.com/) - A newsletter focused on People, Process, and Tools in the DevOps, Cloud Native, and Open Source spaces.
